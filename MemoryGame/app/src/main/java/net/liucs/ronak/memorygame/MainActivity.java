package net.liucs.ronak.memorygame;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements Runnable {

    MediaPlayer chimeSound, backgroundMusic, endingMusic;

    /*@Override
    protected void onPause() {
        super.onPause();
        chimeSound.release();
    }*/

    public static final String LOG_NAME = "MemoryGame";

    private static final String ID_CARDS_REMAINING = "totalNumberOfCards";
    private static final String ID_CARD_VALUES = "cardValues";
    private static final String ID_FACE_UP_INDICES = "faceUpIndices";
    private static final String ID_REMOVED_INDICES = "removedIndices";
    private static final String ID_MUSIC_POSITION = "musicPosition";
    private Handler handler = new Handler();

    private boolean faceDown = true;
    private GridLayout tileGrid;
    private TextView textView;
    private ImageView cardViews[];
    //state of game
    private int totalNumberOfCards;
    private int cardValues[];
    private ArrayList<Integer> faceUpIndices = new ArrayList<Integer>();
    private ArrayList<Integer> removedIndices = new ArrayList<Integer>();
    private int musicPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chimeSound = MediaPlayer.create(this, R.raw.comedy_pop_finger_in_mouth_002);
        backgroundMusic = MediaPlayer.create(this, R.raw.relaxation_music);
        endingMusic = MediaPlayer.create(this, R.raw.musical_piano_003);
        backgroundMusic.setLooping(true);

        tileGrid = (GridLayout) findViewById(R.id.tileGrid);
        textView = (TextView) findViewById(R.id.textView);
        if(savedInstanceState != null) {
            totalNumberOfCards = savedInstanceState.getInt(ID_CARDS_REMAINING);
            cardValues = savedInstanceState.getIntArray(ID_CARD_VALUES);
            faceUpIndices = savedInstanceState.getIntegerArrayList(ID_FACE_UP_INDICES);
            removedIndices = savedInstanceState.getIntegerArrayList(ID_REMOVED_INDICES);
            musicPosition = savedInstanceState.getInt(ID_MUSIC_POSITION);
        }
        else { //Start fresh
            totalNumberOfCards = tileGrid.getRowCount() * tileGrid.getColumnCount();
            cardValues = new int [totalNumberOfCards];
            for(int i = 0; i < totalNumberOfCards; i++) {
                cardValues[i] = i/2;
            }
            shuffle(cardValues);        // SHUFFLING OF CARDS, DEACTIVATE IT BEFORE TESTING //

        }
        cardViews = new ImageView[totalNumberOfCards];
        for (int i = 0; i < totalNumberOfCards; i++) {
            final int j = i;
            cardViews[i] = new ImageView(this);
            cardViews[i].setImageResource(R.drawable.new_card_1);
            if(removedIndices.contains(i)) {
                cardViews[i].setVisibility(View.INVISIBLE);
            }
            cardViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(LOG_NAME, "You clicked on tile #" + j);
                    Log.i(LOG_NAME, "That tile's front side is " + cardValues[j]);
                    // tile.setVisibility(View.INVISIBLE);
                    if(!faceUpIndices.contains(j)     // This card not already face up
                            && faceUpIndices.size() < 2) { // Not yet two cards face up
                        faceUpIndices.add(j);
                        revealCardAtPosition(j);
                        if(faceUpIndices.size() == 2) {
                            handler.postDelayed(MainActivity.this, 850);
                        }
                    }
                }
            });
            tileGrid.addView(cardViews[i]);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(LOG_NAME, "Resuming music at " + musicPosition);
        backgroundMusic.seekTo(musicPosition);
        backgroundMusic.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        backgroundMusic.pause();
        musicPosition = backgroundMusic.getCurrentPosition();
        Log.i(LOG_NAME, "Paused music at " + musicPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backgroundMusic.release();
        chimeSound.release();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ID_CARDS_REMAINING, totalNumberOfCards);
        outState.putIntArray(ID_CARD_VALUES, cardValues);
        outState.putIntegerArrayList(ID_FACE_UP_INDICES, faceUpIndices);
        outState.putIntegerArrayList(ID_REMOVED_INDICES, removedIndices);
        Log.i(LOG_NAME, "Saving instance state.");
        outState.putInt(ID_MUSIC_POSITION, musicPosition);
    }



    @Override
    public void run() {
        // Do they match
        if(cardValues[faceUpIndices.get(0)] ==
                cardValues[faceUpIndices.get(1)])
        {
            chimeSound.start();
            for(int i = 0; i <= 1; i++) {
                cardViews[faceUpIndices.get(i)].setVisibility(View.INVISIBLE);
                removedIndices.add(faceUpIndices.get(i));
            }
        }
        else
        {
            for(int i = 0; i <= 1; i++) {
                cardViews[faceUpIndices.get(i)].setImageResource(R.drawable.new_card_1);
            }
        }
        // Nothing is face up now
        faceUpIndices.clear();
        if(removedIndices.size() == totalNumberOfCards)
        {
            textView.setText("You Won!!");
            backgroundMusic.pause();
            endingMusic.start();
        }


    }

    // from http://www.dotnetperls.com/shuffle-java
    static void shuffle(int[] array) {
        int n = array.length;
        for (int i = 0; i < array.length; i++) {
            // Get a random index of the array past i.
            int random = i + (int) (Math.random() * (n - i));
            // Swap the random element with the present element.
            int randomElement = array[random];
            array[random] = array[i];
            array[i] = randomElement;
        }
    }

    private void revealCardAtPosition(int j) {
        switch(cardValues[j]) {
            case 0: cardViews[j].setImageResource(R.drawable.c02); break;
            case 1: cardViews[j].setImageResource(R.drawable.c03); break;
            case 2: cardViews[j].setImageResource(R.drawable.c04); break;
            case 3: cardViews[j].setImageResource(R.drawable.c05); break;
            case 4: cardViews[j].setImageResource(R.drawable.c06); break;
            case 5: cardViews[j].setImageResource(R.drawable.c07); break;
            case 6: cardViews[j].setImageResource(R.drawable.c08); break;
            case 7: cardViews[j].setImageResource(R.drawable.c09); break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**public void playMusic(View view) {
     chimeSound.start();
     chimeSound.setLooping(true);
     }*/

}