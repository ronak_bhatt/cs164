package net.liucs.ronak.game;

/**
 * Created by RonakB on 5/2/15.
 */
import android.content.Context;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;


public class GameMusic {

    private ArrayList<MediaPlayer> players = new ArrayList<MediaPlayer>();
    private int idxBackground, idxGoodMatch, idxBadMatch,
            idxGoodGame, idxEndingMusic;

    public GameMusic(Context context) {
        idxBackground = players.size();
        players.add(MediaPlayer.create(context, R.raw.relaxation_music));
        players.get(idxBackground).setLooping(true);

        idxGoodMatch = players.size();
        players.add(MediaPlayer.create(context, R.raw.comedy_pop_finger_in_mouth_002));

        idxBadMatch = players.size();
        players.add(MediaPlayer.create(context,R.raw.beep));

        idxEndingMusic = players.size();
        players.add(MediaPlayer.create(context,R.raw.background_music_02));
    }

    public void releaseAll() {
        for(MediaPlayer p : players) {
            p.release();
        }
    }

    public void playMatchEffect(boolean success) {
        players.get(success? idxGoodMatch : idxBadMatch).start();
    }

    public void playGameOver(boolean success) {
        Log.i("MUSIC", "SKIP playing game-over at slot #" + idxGoodGame);
        players.get(success? idxEndingMusic : idxEndingMusic).start();
    }

    public int pause() {
        Log.i("MUSIC", "Pausing music slot #" + idxBackground);
        players.get(idxBackground).pause();
        return players.get(idxBackground).getCurrentPosition();
    }

    public void resume(int position) {
        players.get(idxBackground).seekTo(position);
        players.get(idxBackground).start();
    }
}