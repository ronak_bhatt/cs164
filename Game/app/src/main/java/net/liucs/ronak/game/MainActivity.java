package net.liucs.ronak.game;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.widget.ImageView;
import android.media.MediaPlayer;


public class MainActivity extends ActionBarActivity implements View.OnClickListener, Runnable {

    private Button button1;
    private Button button2;
    private Button button3;
    private ImageView gameIcon;

    MediaPlayer startingMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        gameIcon = (ImageView) findViewById(R.id.imageView);

        startingMusic = MediaPlayer.create(this, R.raw.background_music_02);

    }

    @Override
    public void onClick(View v) {

        if (v == button1)
        {
            Log.i("Demo", "You Clicked on 1 !!");
            Intent i = new Intent(this, Memory_Game.class);
            startActivity(i);

        }
        else if (v == button2)
        {
            Log.i("Demo", "You Clicked on 2 !!");
            Intent i = new Intent(this, High_Scores.class);
            startActivity(i);

        }
        else if (v == button3)
        {
            Log.i("Demo", "You Clicked on 3 !!");
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void run() {

        startingMusic.start();
        startingMusic.setLooping(true);

    }

    @Override

    protected void onResume() {
        super.onResume();
       startingMusic.start();
       startingMusic.setLooping(true);
    }

    @Override

    protected void onPause() {
        super.onPause();
       startingMusic.pause();
    }

    @Override

    protected void onDestroy() {
        super.onDestroy();
       startingMusic.release();
    }
}
