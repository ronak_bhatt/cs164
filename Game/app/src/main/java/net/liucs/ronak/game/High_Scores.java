package net.liucs.ronak.game;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.Random;


public class High_Scores extends ActionBarActivity
        implements Runnable {


    static final String LOG_NAME = "HighScores";
    static final String FETCH_SCORES_URL = "http://cs120.liucs.net/scores/Ronak";
    private ProgressBar busyIndicator;
    private LinearLayout scoresList;
    private Handler handler = new Handler();
    private Random rng = new Random();
    private TextView randomTextView;
    int random = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high__scores);
        busyIndicator = (ProgressBar) findViewById(R.id.busyIndicator);
        scoresList = (LinearLayout) findViewById(R.id.scoresList);
        randomTextView = (TextView) findViewById(R.id.randomScoreText);
        Button refreshButton = (Button) findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronizeScores();
            }
        });
        Button postButton = (Button) findViewById(R.id.postButton);
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postScore();
            }
        });
        handler.postDelayed(this, 500);
    }


    @Override
    public void run() {
        random = rng.nextInt(100);
        randomTextView.setText("" + random);
        handler.postDelayed(this, 500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        synchronizeScores();
    }

    private void postScore() {
        AsyncTask<String, Void, Void> task = new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {
                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(params[0]);
                try {
                    client.execute(post);
                } catch(Exception exn) {
                    Log.i(LOG_NAME, "postScore: " + exn);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                synchronizeScores();
            }
        };
        task.execute(FETCH_SCORES_URL + "/Ronak/" + random);
    }

    private void synchronizeScores() {
        AsyncTask<String, Void, JSONArray> task = new AsyncTask<String, Void, JSONArray>() {
            @Override
            // This happens on background thread, so cannot touch the UI
            protected JSONArray doInBackground(String... params) {
                Log.i(LOG_NAME, "In background: fetch " + params[0]);

                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(params[0]);
                try {
                    HttpResponse response = client.execute(get);
                    HttpEntity entity = response.getEntity();
                    // Read entity into string
                    InputStream input = entity.getContent();
                    BufferedReader bReader = new BufferedReader(new InputStreamReader(input, "utf-8"), 8);
                    StringBuilder sBuilder = new StringBuilder();

                    String line = null;
                    while ((line = bReader.readLine()) != null) {
                        sBuilder.append(line + "\n");
                    }
                    input.close();
                    return new JSONArray(sBuilder.toString());
                } catch(Exception exn) {
                    Log.i(LOG_NAME, "EXCEPTION: " + exn);
                    return new JSONArray();
                }
            }

            @Override
            protected void onPreExecute() {
                busyIndicator.setVisibility(View.VISIBLE);
            }

            @Override // Happens on the UI thread
            protected void onPostExecute(JSONArray a) {
                Log.i(LOG_NAME, "Post execute: got " +
                        a.length() + " scores.");
                scoresList.removeAllViews();
                try {
                    for (int i = 0; i < a.length(); i++) {
                        TextView tv = new TextView(High_Scores.this);
                        JSONObject obj = a.getJSONObject(i);
                        String player = obj.getString("player");
                        int score = obj.getInt("score");
                        tv.setText("" + (i + 1) + ". " + player + " " + score);
                        scoresList.addView(tv);
                    }
                }
                catch(Exception exn) {
                    Log.i(LOG_NAME, "JSON error? " + exn);
                }
                busyIndicator.setVisibility(View.INVISIBLE);
            }
        };
        task.execute(FETCH_SCORES_URL);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}