package net.liucs.ronak.game;

/**
 * Created by RonakB on 5/2/15.
 */

import junit.framework.TestCase;

public class GameStateTest extends TestCase {

    private GameState gs;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        gs = new GameState(0, 32);
    }

    public void testSomething() {
        // After clicking two cards, we are ready to match.
        assertTrue(gs.turnCardOver(0));
        assertTrue(gs.turnCardOver(1));
        assertTrue(gs.readyToMatch());
    }

    public void testAnother() {
        // Clicking the same card twice is not allowed.
        assertTrue(gs.turnCardOver(0));
        assertFalse(gs.turnCardOver(0));
    }
}