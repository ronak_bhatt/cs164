package net.liucs.ronak.mortgage;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import org.achartengine.ChartFactory;
import org.achartengine.chart.PieChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class MainActivity extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener{

    private static final String LOG_NAME = "Mortgage";

    private static final int minPrice = 100000;
    private static final int maxPrice = 2500000;
    private static final double minRate = 0.01;
    private static final double maxRate = 0.10;
    private static final double minDown = 0.05;
    private static final double maxDown = 0.50;
    private static final int loanMonths = 360;

    private SeekBar priceSeek;
    private SeekBar rateSeek;
    private SeekBar downSeek;
    private TextView priceAmount, rateAmount, downAmount, textView;
    private TextView monthlyPayText, totalPaidText, interestPaidText;
    private LinearLayout chartContainer;
    private DefaultRenderer renderer;

    private NumberFormat currencyFormat, percentFormat;

    private double currentPrice;
    private double currentDownpct;
    private double currentAnnualRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        currencyFormat = NumberFormat.getCurrencyInstance();
        percentFormat = NumberFormat.getPercentInstance();
        percentFormat.setMinimumFractionDigits(2);

        priceAmount = (TextView) findViewById(R.id.priceAmount);
        rateAmount = (TextView) findViewById(R.id.rateAmount);
        downAmount = (TextView) findViewById(R.id.downAmount);
        textView = (TextView) findViewById(R.id.textView);
        monthlyPayText = (TextView) findViewById(R.id.textMonthly);
        totalPaidText = (TextView) findViewById(R.id.textTotalPaid);
        interestPaidText = (TextView) findViewById(R.id.textInterestPaid);
        chartContainer = (LinearLayout) findViewById(R.id.chartContainer);

        priceSeek = (SeekBar) findViewById(R.id.priceSeek);
        priceSeek.setOnSeekBarChangeListener(this);
        rateSeek = (SeekBar) findViewById(R.id.rateSeek);
        rateSeek.setOnSeekBarChangeListener(this);
        downSeek = (SeekBar) findViewById(R.id.downSeek);
        downSeek.setOnSeekBarChangeListener(this);

        Log.i(LOG_NAME, "Creating renderer");
        int[] colors = {Color.RED, Color.YELLOW, Color.GRAY};
        renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(40);
        for (int c : colors) {
            SimpleSeriesRenderer ssr = new SimpleSeriesRenderer();
            ssr.setShowLegendItem(false);
            ssr.setColor(c);                        // new line //
            renderer.addSeriesRenderer(ssr);
        }

        if (savedInstanceState == null) {
            priceSeek.setProgress(priceSeek.getMax() / 3);
            rateSeek.setProgress(rateSeek.getMax() / 3);
            downSeek.setProgress(downSeek.getMax() / 3);
        }
    }

    private void recalculate()
    {

        Log.i(LOG_NAME, "Recalculating...");
        double principal = currentPrice * (1 - currentDownpct);
        double monthlyPayment = InterestFormula.payment(principal,currentAnnualRate,loanMonths);
         monthlyPayText.setText(currencyFormat.format(monthlyPayment));
        double totalPaid = monthlyPayment * loanMonths;
        totalPaidText.setText(currencyFormat.format(totalPaid));
        double interestPaid = totalPaid - principal;
        interestPaidText.setText(currencyFormat.format(interestPaid));

        CategorySeries data  = new CategorySeries("Chart");
        data.add("Downpayment", currentPrice * currentDownpct);
        data.add("Principal", principal);
        data.add("Interest", interestPaid);

        chartContainer.removeAllViews();
        chartContainer.addView(ChartFactory.getPieChartView(this, data, renderer));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if(seekBar == priceSeek)
        {
            currentPrice = InterestFormula.mapToRange(progress,seekBar.getMax(), minPrice,maxPrice);
            priceAmount.setText(currencyFormat.format(currentPrice));
        }

        else if(seekBar == rateSeek)
        {
            currentAnnualRate = InterestFormula.mapToRange(progress, seekBar.getMax(), minRate, maxRate);
            rateAmount.setText(percentFormat.format(currentAnnualRate));
        }

        else if(seekBar == downSeek)
        {
            currentDownpct = InterestFormula.mapToRange(progress, seekBar.getMax(), minDown,maxDown);
        }

        if( seekBar == priceSeek || seekBar == downSeek)
        {
            double downDollars = currentDownpct * currentPrice;
            downAmount.setText(percentFormat.format(currentDownpct) + " ("
                    + currencyFormat.format(downDollars) + ")");
        }

        recalculate();

        Log.i(LOG_NAME, "changed to " + progress);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
