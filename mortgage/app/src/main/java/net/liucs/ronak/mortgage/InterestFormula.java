package net.liucs.ronak.mortgage;

/**
 * Created by RonakB on 3/19/15.
 */
public class InterestFormula {



    public static double payment(double principal, double annualRate, int numPayments){
        // M  = P [i (1 + i)^n ] / [ (1 + i)^n -1 ]
        double i = annualRate / 12;
        double e = Math.pow(1+i, numPayments);
        return principal * i * e / (e - 1);
    }

    public static double mapToRange(double current, double outOf, double min, double max){

        double pct = current / outOf;
        double range = max - min;
        return min + pct * range;
    }
}
