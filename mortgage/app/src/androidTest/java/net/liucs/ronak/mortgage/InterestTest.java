package net.liucs.ronak.mortgage;

/**
 * Created by RonakB on 3/19/15.
 */
public class InterestTest extends TestCase {

    public void testPayment1(){
        double m = InterestFormula.payment(400000, 0.35, 360);
        assertEquals(1796.27, m, 0.1);
    }

    public void testRange(){
        double m  = InterestFormula.mapToRange(15, 100, 10, 60);
        assertEquals(17.5, m);
    }

    public void testRange2(){
        double m = InterestFormula.mapToRange(31, 50, 0.03, 0.75);
        assertEquals(0.4764, m, 0.0001);
    }
}
