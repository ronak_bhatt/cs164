package net.liucs.ronak.my_first_application;

import junit.framework.TestCase;


public class CalculationTest extends TestCase {
    public void testOddsTo5(){
        assertEquals(9, Calculation.SumOddsUpTo(5));
    }
    public void testOddsNeg(){
        try{
            int x = Calculation.SumOddsUpTo(-9);
            fail("You should have raised expection.");
        }catch (IllegalArgumentException e){
            //good
        }
    }
    public void testOddsto0() {assertEquals(0,Calculation.SumOddsUpTo(0));}

    public void testAssertion(){assertEquals(5, 2+3);}
}
