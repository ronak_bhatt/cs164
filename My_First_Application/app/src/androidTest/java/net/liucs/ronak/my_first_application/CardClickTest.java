package net.liucs.ronak.my_first_application;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class CardClickTest extends ActivityInstrumentationTestCase2<MainActivity> {


    private MainActivity activity;
    private Button button;
    private TextView helloText;

    public CardClickTest(Class<MainActivity> activityClass) {
        super(MainActivity.class);

    }
    @Override
    public void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        button = (Button) activity.findViewById(R.id.button);
        helloText = (TextView) activity.findViewById(R.id.helloText);

    }

    public void testSomething(){
        assertEquals(4, 2+2);
    }

    public void testHello() {
        assertEquals("Hello World!", helloText.getText());
    }
    @UiThreadTest
    public void testPressMe(){

        assertEquals("Hello World!", helloText.getText());
        button.performClick();
        assertEquals("You pressed me!", helloText.getText());
    }
}
