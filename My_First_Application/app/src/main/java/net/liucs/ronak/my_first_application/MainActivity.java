package net.liucs.ronak.my_first_application;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements Runnable{

    public static final String LOG_NAME = "Memory Game";
    private Handler handler = new Handler();
    private GridLayout tileGrid;
    private Button button;
    private TextView helloText;
    private boolean faceDown = true;
    private ImageView cardViews[];
    private int cardValues[];
    private int firstFaceUp = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        helloText = (TextView) findViewById(R.id.helloText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                helloText.setText("You pressed me!");
            }
        });
        tileGrid = (GridLayout) findViewById(R.id.tileGrid);

        int n = tileGrid.getColumnCount() * tileGrid.getRowCount();
        cardValues = new int [n];

        // to do randomize the tile//
        for (int i = 0; i < n; i ++)
        {
            cardValues[i] = i/2;
        }

        shuffle(cardValues);

        Log.i(LOG_NAME, "Row is " + tileGrid.getRowCount());
        Log.i(LOG_NAME, "Column is " + tileGrid.getColumnCount());
        Log.i(LOG_NAME, "Creating " + n + "Card Values.");
        cardValues = new ImageView[n];

        for (int i = 0; i < n ; i ++)
        {
            final int j = i;
            Log.i(LOG_NAME, "Created tile # " +i);
            cardViews [i] = new ImageView(this);
            final ImageView tile = new ImageView(this);
            cardViews[i].setImageResource(R.drawable.icard1);
            cardViews[i].setOnClickListener((v) -> {
                    Log.i(LOG_NAME, "You Clicked on tile # " + j);
                    Log.i(LOG_NAME, "That tile front side is: " + cardValues[j]);


              revealCardAtPosition(j);
             firstFaceUp = j;
             handler.postDelayed(MainActivity.this, 2000);
            });

            tileGrid.addView(cardViews[i]);

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void run() {
        cardViews[firstFaceUp].setImageResource(R.drawable.icard1);
        firstFaceUp = -1;
    }

    static void shuffle(int[] array) {
        int n = array.length;
        for (int i = 0; i < array.length; i++) {
            // Get a random index of the array past i.
            int random = i + (int) (Math.random() * (n - i));
            // Swap the random element with the present element.
            int randomElement = array[random];
            array[random] = array[i];
            array[i] = randomElement;
        }
    }




    private void revealCardAtPosition(int j){
        switch (cardValues[j]){
            case 0: cardViews[j].setImageResource(R.drawable.c02);
                break;
            case 1: cardViews[j].setImageResource(R.drawable.c03);
                break;
            case 2: cardViews[j].setImageResource(R.drawable.c04);
                break;
            case 3: cardViews[j].setImageResource(R.drawable.c05);
                break;
            case 4: cardViews[j].setImageResource(R.drawable.c06);
                break;
            case 5: cardViews[j].setImageResource(R.drawable.c07);
                break;
            case 6: cardViews[j].setImageResource(R.drawable.c08);
                break;
            case 7: cardViews[j].setImageResource(R.drawable.c09);
                break;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


